<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{
    protected $fillable = [
	    'request_id',
	    'reference',
	    'customer_name',
	    'customer_email',
	    'customer_mobile',
	    'product_sku',
	    'total',
	    'status',
	    'payment_method',
	    'process_url'
	  ];
}
