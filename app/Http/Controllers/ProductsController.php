<?php

namespace App\Http\Controllers;
 
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Product;
use App\Transaction;
use Dnetix\Redirection\PlacetoPay;

class ProductsController extends Controller
{
    //Lista los productos en el home
    public function index()
    {
        $products = Product::all();
 
        return view('products', compact('products'));
    }


    //Página de carrito
    public function cart()
    {
        return view('cart');
    }

    //Añadir los productos al carrito
	public function addToCart($id)
    {
        $product = Product::find($id);
 
        if(!$product) {
 
            abort(404);
 
        }
 
        $cart = session()->get('cart');
 
        // if cart is empty then this the first product
        if(!$cart) {
 
            $cart = [
                    $id => [
                        "sku" => $product->sku,
                        "name" => $product->name,
                        "quantity" => 1,
                        "price" => $product->price,
                        "photo" => $product->photo
                    ]
            ];
 
            session()->put('cart', $cart);
 
            return redirect()->back()->with('success', 'Product added to cart successfully!');
        }
 
        // if cart not empty then check if this product exist then increment quantity
        if(isset($cart[$id])) {
 
            $cart[$id]['quantity']++;
 
            session()->put('cart', $cart);
 
            return redirect()->back()->with('success', 'Product added to cart successfully!');
 
        }
 
        // if item not exist in cart then add to cart with quantity = 1
        $cart[$id] = [
            "sku" => $product->sku,
            "name" => $product->name,
            "quantity" => 1,
            "price" => $product->price,
            "photo" => $product->photo
        ];
 
        session()->put('cart', $cart);
 
        return redirect()->back()->with('success', 'Product added to cart successfully!');
    }


    //Actualizar el precio dependiendo de la cantidad
	public function update(Request $request)
    {
        if($request->id and $request->quantity)
        {
            $cart = session()->get('cart');
 
            $cart[$request->id]["quantity"] = $request->quantity;
 
            session()->put('cart', $cart);
 
            session()->flash('success', 'Cart updated successfully');
        }
    }
 
    //Elimina los productos del carrito
    public function remove(Request $request)
    {
        if($request->id) {
 
            $cart = session()->get('cart');
 
            if(isset($cart[$request->id])) {
 
                unset($cart[$request->id]);
 
                session()->put('cart', $cart);
            }
 
            session()->flash('success', 'Product removed successfully');
        }
    }

    //Redirección a la pasarela de Place to Pay
    public function redirectionCheckout(Request $request)
    {
        //Conexión a Place to Pay
    	$placetopay = new PlacetoPay([
		    'login' => '6dd490faf9cb87a9862245da41170ff2',
		    'tranKey' => '024h1IlD',
		    'url' => 'https://dev.placetopay.com/redirection/',
		]);

        //Envío de datos del comprador a Place to Pay
    	$nombre = $request->input('nombre');
    	$email = $request->input('email');
    	$celular = $request->input('celular');
    	$nombre_producto = $request->input('nombre_producto');
    	$sku = $request->input('sku');
    	$cantidad = $request->input('cantidad');
    	$precio = $request->input('precio');
    	$total = $request->input('total');
    	$time = date('Y-m-d-H-i-s');

		$reference = $sku.'_'.$time;
		$request = [
		    "buyer" => [
		        "name" => $nombre,
		        //"surname" => "Yost",
		        "email" => $email,
		        //"documentType" => "CC",
		        //"document" => "1848839248",
		        "mobile" => $celular
		        /*"address" => [
		            "street" => "703 Dicki Island Apt. 609",
		            "city" => "North Randallstad",
		            "state" => "Antioquia",
		            "postalCode" => "46292",
		            "country" => "US",
		            "phone" => "363-547-1441 x383"
		        ]*/
		    ],
		    'payment' => [
		        'reference' => $reference,
		        'description' => 'Pago prueba Place to Pay',
		        'amount' => [
		            'currency' => 'COP',
		            'total' => $total,
		        ],
		    ],
		    "items" => [
	            [
	                "sku" => $sku,
	                "name" => $nombre_producto,
	                //"category" => "physical",
	                "qty" => $cantidad,
	                "price" => $precio
	                //"tax" => 89.3
	            ]
	        ],
		    'expiration' => date('c', strtotime('+2 days')),
		    'returnUrl' => 'http://127.0.0.1:8000/response?reference=' . $reference,
		    'ipAddress' => '127.0.0.1',
		    'userAgent' => 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/52.0.2743.116 Safari/537.36',
		];

		$response = $placetopay->request($request);

		//dd($response);
		if ($response->isSuccessful()) {
		    // STORE THE $response->requestId() and $response->processUrl() on your DB associated with the payment order
		    // Redirect the client to the processUrl or display it on the JS extension
		    
            //Se crea la transacción en la base de datos
		    $transaction = new Transaction([
		        'request_id' => $response->requestId(),
		        'reference' => $reference,
		        'customer_name' => $nombre,
		        'customer_email' => $email,
		        'customer_mobile' => $celular,
		        'product_sku' => $sku,
		        'total' => $total,
                'process_url' => $response->processUrl()
		    ]);
		    $transaction->save();

		    return response()->json(['response' => 'success', 'proccess_url' => $response->processUrl()]);
		} else {
		    // There was some error so check the message and log it
		    $response->status()->message();
		}
    }

    //Consulta el estado de la transacción y redirige al usuario a la página de confirmación
    public function statusCheckout(Request $request)
    {
        //Se realiza la conexión a Place to Pay
    	$placetopay = new PlacetoPay([
		    'login' => '6dd490faf9cb87a9862245da41170ff2',
		    'tranKey' => '024h1IlD',
		    'url' => 'https://dev.placetopay.com/redirection/',
		]);

		$transaction = Transaction::join('products', 'products.sku', 'transactions.product_sku')
        ->select('transactions.*', 'products.sku', 'products.name', 'products.price')
        ->where('reference', $request->input('reference') )
        ->first();

    	$response = $placetopay->query($transaction->request_id);

		if ($response->isSuccessful()) {
		    // In order to use the functions please refer to the Dnetix\Redirection\Message\RedirectInformation class

            //Actualizamos el estado de la transacción y el método de pago
			$transactionUpdate = Transaction::where('request_id', $transaction->request_id)->first();
		    $transactionUpdate->status = $response->status()->status();
            $transactionUpdate->payment_method = $response->payment()[0]->paymentMethodName();
		    $transactionUpdate->save();

		    return view('notification', compact('transaction'));
		} else {
		    // There was some error with the connection so check the message
		    print_r($response->status()->message() . "\n");
		}
    }

    //Consuta las transacciones en estado pendiente y las actualiza cada determinado tiempo
    public function statusSonda(Request $request)
    {
        //Se realiza la conexión a Place to Pay
        $placetopay = new PlacetoPay([
            'login' => '6dd490faf9cb87a9862245da41170ff2',
            'tranKey' => '024h1IlD',
            'url' => 'https://dev.placetopay.com/redirection/',
        ]);

        $transactions = Transaction::all();

        //dd($transactions);

        foreach ($transactions as $transaction) {

            //dd($transaction);
            $response = $placetopay->query($transaction->request_id);

            if ($response->isSuccessful()) {
                // In order to use the functions please refer to the Dnetix\Redirection\Message\RedirectInformation class

                //Actualizamos el estado de la transacción y el método de pago
                $transactionUpdate = Transaction::where('request_id', $transaction->request_id)->first();
                $transactionUpdate->status = $response->status()->status();
                $transactionUpdate->payment_method = $response->payment()[0]->paymentMethodName();
                $transactionUpdate->save();

                print('realizado');

            } else {
                // There was some error with the connection so check the message
                print_r($response->status()->message() . "\n");
            }
        }

    }


}
