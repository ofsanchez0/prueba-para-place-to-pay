<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transactions', function (Blueprint $table) {
            $table->increments('id');
            $table->string("request_id", 30);
            $table->string("reference", 200)->nullable();
            $table->string("customer_name", 80)->nullable();
            $table->string("customer_email", 120)->nullable();
            $table->string("customer_mobile", 40)->nullable();
            $table->string("product_sku", 100)->nullable();
            $table->string("total", 30)->nullable();
            $table->string("status", 40)->nullable();
            $table->string("payment_method", 120)->nullable();
            $table->string("process_url", 500)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transactions');
    }
}
