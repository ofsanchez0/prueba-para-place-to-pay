<?php

use Illuminate\Database\Seeder;

class ProductsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
		DB::table('products')->insert([
           'SU' => '1111111',
           'name' => 'Lo demás es loma - Negra',
           'description' => 'Camiseta alusiva a la caleñidad.',
           'photo' => 'camiseta_lo_demas_es_loma.jpg',
           'price' => 39900.00
        ]);
    }
}
