<!DOCTYPE html>
<html>
<head>
 
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
 
    <title>@yield('title')</title>
 
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="{{ asset('material')}}/css/style.css">
 
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
    
 
</head>
<body>
<header>
    
    <div class="header-one">
        <div class="container">
            <div class="row">
                <div class="col-lg-10 col-sm-10 col-12">
                Transporte en Colombia gratuito en órdenes superiores a 500.000 COP - WhatsApp (+57) 3206739191
                </div>
                <div class="col-lg-1 col-sm-1 col-12 col-one">
                    <a href="#">BOLETÍN</a>
                </div>
                <div class="col-lg-1 col-sm-1 col-12 col-two">
                    <a href="#">CONTACTO</a>
                </div>
            </div>
        </div>
    </div>

    <div class="header-two">

        <div class="container">
         
            <div class="row">
                <div class="col-lg-3 col-sm-3 col-12">
                    <div class="logo">
                        <a href="/"><img src="{{ asset('material')}}/img/logo.png"></a>
                    </div>
                </div>
                <div class="col-lg-9 col-sm-9 col-12 main-section">
                    <div class="dropdown">
                        <button type="button" class="btn btn-info cart-button" data-toggle="dropdown">
                            <i class="fa fa-shopping-cart" aria-hidden="true"></i> Carrito <span class="badge badge-pill badge-danger">{{ count((array) session('cart')) }}</span>
                        </button>
                        <div class="dropdown-menu">
                            <div class="row total-header-section">
                                <div class="col-lg-6 col-sm-6 col-6">
                                    <i class="fa fa-shopping-cart" aria-hidden="true"></i> <span class="badge badge-pill badge-danger">{{ count((array) session('cart')) }}</span>
                                </div>
         
                                <?php $total = 0 ?>
                                @foreach((array) session('cart') as $id => $details)
                                    <?php $total += $details['price'] * $details['quantity'] ?>
                                @endforeach
         
                                <div class="col-lg-6 col-sm-6 col-6 total-section text-right">
                                    <p>Total: <span class="text-info">$ {{ $total }}</span></p>
                                </div>
                            </div>
         
                            @if(session('cart'))
                                @foreach(session('cart') as $id => $details)
                                    <div class="row cart-detail">
                                        <div class="col-lg-4 col-sm-4 col-4 cart-detail-img">
                                            <img src="{{ asset('material') }}/img/products/{{ $details['photo'] }}" />
                                        </div>
                                        <div class="col-lg-8 col-sm-8 col-8 cart-detail-product">
                                            <p>{{ $details['name'] }}</p>
                                            <span class="price text-info"> ${{ $details['price'] }}</span> <span class="count"> Cantidad:{{ $details['quantity'] }}</span>
                                        </div>
                                    </div>
                                @endforeach
                            @endif
                            <div class="row">
                                <div class="col-lg-12 col-sm-12 col-12 text-center checkout">
                                    <a href="{{ url('cart') }}" class="btn btn-primary btn-block">VER CARRITO</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>

</header>
 
<div class="container page">
    @yield('content')
</div>

<footer>

    <div class="footer-one">
        <div class="container">
            <div class="row">
                <div class="col-lg-3 col-sm-3 col-12">
                    <div class="widget widget-one">
                        <h4>Envío gratuito</h4>
                        <h5>Compras superiores a 500.000</h5>
                    </div>
                </div>
                <div class="col-lg-3 col-sm-3 col-12">
                    <div class="widget widget-two">
                        <h4>Soporte</h4>
                        <h5>Lun - Vie 8:00 - 17:00</h5>
                    </div>
                </div>
                <div class="col-lg-3 col-sm-3 col-12">
                    <div class="widget widget-tree">
                        <h4>Pago online.</h4>
                        <h5>Rápido y seguro.</h5>
                    </div>
                </div>
                <div class="col-lg-3 col-sm-3 col-12">
                    <div class="widget widget-four">
                        <h4>Despacho de productos.</h4>
                        <h5>Preparamos su pedido en 3 días.</h5>
                    </div>
                </div>
            </div>
        </div>
    </div>

    
    <div class="footer-two">
        <div class="container">
            <div class="row">
                <div class="col-lg-4 col-sm-4 col-12">
                    <div class="widget widget-one">
                        <div class="logo">
                            <a href="/"><img src="{{ asset('material')}}/img/logo_blanco.png"></a>
                        </div>
                        <p>
                            Esta es una tienda de pruebas para el cargo de desarrollador ofertado por la empresa Place to Pay.
                        </p>

                        <p>
                            Diseñada y desarrollada por Oscar Fernando Sánchez Martinez
                            Analista y Desarrollador Frontend y Backend.
                        </p>
                    </div>
                </div>
                <div class="col-lg-4 col-sm-4 col-12">
                    <div class="widget widget-one">
                        <h3>UBICACIÓN</h3>
                        <div class="mapa">
                            <img src="{{ asset('material')}}/img/mapa.png">
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-sm-4 col-12">
                    <div class="widget widget-one">
                        <h3>ENLACES DE INTERÉS</h3>
                        <ul class="menu-footer">
                            <li><a href="#"><i class="fas fa-chevron-right"></i> Preguntas frecuentes</a></li>
                            <li><a href="#"><i class="fas fa-chevron-right"></i> Condiciones de garantía</a></li>
                            <li><a href="#"><i class="fas fa-chevron-right"></i> Políticas de envío</a></li>
                            <li><a href="#"><i class="fas fa-chevron-right"></i> Contacto</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="footer-tree">
        <div class="container">
            <div class="row">
                <div class="col-lg-3 col-sm-3 col-12">
                    <div class="widget widget-one">
                        <div class="logo-place-to-pay">
                            <a href="https://www.placetopay.com/web/home" target="new"><img src="{{ asset('material')}}/img/logo_place_to_pay.png"></a>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-sm-3 col-12">
                    <div class="widget widget-tree">
                    </div>
                </div>
                <div class="col-lg-6 col-sm-6 col-12">
                    <div class="widget widget-tree">
                        <div class="medios_pago">
                            <a href="#" target="new"><img src="{{ asset('material')}}/img/medios_pago.png"></a>
                        </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</footer>
@yield('scripts')
 
</body>
</html>