<div class="sidebar" data-color="orange" data-background-color="white" data-image="{{ asset('material') }}/img/sidebar-1.jpg">
  <!--
      Tip 1: You can change the color of the sidebar using: data-color="purple | azure | green | orange | danger"

      Tip 2: you can also add an image using data-image tag
  -->
  <div class="logo">
    <a href="#" class="simple-text logo-normal">
      <img src="{{ asset('material')}}/img/logo.png">
    </a>
  </div>
  <div class="sidebar-wrapper">
    <ul class="nav">
      <li class="nav-item{{ $activePage == 'dashboard' ? ' active' : '' }}">
        <a class="nav-link" href="{{ route('home') }}">
          <i class="material-icons">dashboard</i>
            <p>{{ __('Dashboard') }}</p>
        </a>
      </li>

      <li class="nav-item{{ $activePage == 'productos-list' ? ' active' : '' }}">
        <a class="nav-link" href="{{ route('productos.index') }}">
          <i class="material-icons">dvr</i>
            <p>{{ __('Productos') }}</p>
        </a>
      </li>

      <li class="nav-item{{ $activePage == 'pedidos-list' ? ' active' : '' }}">
        <a class="nav-link" href="{{ route('pedidos.index') }}">
          <i class="material-icons">dvr</i>
            <p>{{ __('Pedidos') }}</p>
        </a>
      </li>
      
    </ul>
  </div>
</div>