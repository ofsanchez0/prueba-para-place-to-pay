@extends('layout')
 
@section('title', 'Products')
 
@section('content')
 
    <div class="container products">
 
        <div class="row">
 
            @foreach($products as $product)
                <div class="col-sm-4">
                </div>
                <div class="col-sm-4">
                    <div class="thumbnail">
                        <img src="{{ asset('material') }}/img/products/{{ $product->photo }}" width="500" height="300">
                        <div class="caption">
                            <h4>{{ $product->name }}</h4>
                            <p>{{ $product->description }}</p>
                            <p><strong>Precio: </strong> {{ $product->price }}$</p>
                            <p class="btn-holder"><a href="{{ url('add-to-cart/'.$product->id) }}" class="btn btn-warning btn-block text-center" role="button">Agregar al carrito</a> </p>
                        </div>
                    </div>
                </div>
                <div class="col-sm-4">
                </div>
            @endforeach
 
        </div><!-- End row -->
 
    </div>
 
@endsection