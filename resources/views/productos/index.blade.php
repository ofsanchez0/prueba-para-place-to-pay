@extends('layouts.app', ['activePage' => 'productos-list', 'titlePage' => __('Gestionar productos')])

@section('content')
  <div class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-md-12">
            <div class="card">
              <div class="card-header card-header-primary">
                <h4 class="card-title ">{{ __('productos') }}</h4>
                <p class="card-category"> {{ __('Gestionar productos') }}</p>
              </div>
              <div class="card-body">
                @if (session()->get('success'))
                  <div class="row">
                    <div class="col-sm-12">
                      <div class="alert alert-success">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                          <i class="material-icons">close</i>
                        </button>
                        <span>{{ session()->get('success') }}</span>
                      </div>
                    </div>
                  </div>
                @endif
                <div class="row">
                  <div class="col-12 text-right">
                    <a href="#" class="btn btn-sm btn-primary">{{ __('Agregar producto') }}</a>
                  </div>
                </div>
                <div class="table-responsive">
                  <table class="table" id="tabla_producto">
                    <thead class=" text-primary">
                      <th>
                          {{ __('Sku') }}
                      </th>
                      <th>
                          {{ __('Nombre') }}
                      </th>
                      <th>
                          {{ __('Descripción') }}
                      </th>
                      <th>
                          {{ __('Precio') }}
                      </th>
                      <th>
                        {{ __('Fecha de creación') }}
                      </th>
                      <th class="text-right">
                        {{ __('Acciones') }}
                      </th>
                    </thead>
                    <tbody>
                      @foreach($productos as $producto)
                        <tr>
                          <td>
                            {{ $producto->sku }}
                          </td>
                          <td>
                            {{ $producto->name }}
                          </td>
                          <td>
                            {{ $producto->description }}
                          </td>
                          <td>
                            {{ $producto->price }}
                          </td>
                          <td>
                            {{ $producto->created_at }}
                          </td>
                          <td class="td-actions text-right">
                              <form action="{{ route('productos.destroy', $producto->id)}}" style="display: inline-block;" method="post">
                              @csrf
                              @method('delete')
                          
                              <!-- <a rel="tooltip" class="btn btn-success btn-link" href="{{ route('productos.edit', $producto) }}" data-original-title="" title="">
                                <i class="material-icons">edit</i>
                                <div class="ripple-container"></div>
                              </a> -->
                              <button type="button" class="btn btn-danger btn-link" data-original-title="" title="" onclick="confirm('{{ __("Estas seguro que deseas eliminar la producto?") }}') ? this.parentElement.submit() : ''">
                                  <i class="material-icons">close</i>
                                  <div class="ripple-container"></div>
                              </button>
                             </form>
                          </td>
                        </tr>
                      @endforeach
                    </tbody>
                  </table>
                </div>
              




            </div>
        </div>
      </div>
    </div>
  </div>
@endsection