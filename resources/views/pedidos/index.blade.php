@extends('layouts.app', ['activePage' => 'pedidos-list', 'titlePage' => __('Gestionar pedidos')])

@section('content')
  <div class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-md-12">
            <div class="card">
              <div class="card-header card-header-primary">
                <h4 class="card-title ">{{ __('pedidos') }}</h4>
                <p class="card-category"> {{ __('Gestionar pedidos') }}</p>
              </div>
              <div class="card-body">
                @if (session()->get('success'))
                  <div class="row">
                    <div class="col-sm-12">
                      <div class="alert alert-success">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                          <i class="material-icons">close</i>
                        </button>
                        <span>{{ session()->get('success') }}</span>
                      </div>
                    </div>
                  </div>
                @endif
                <div class="row">
                  <div class="col-12 text-right">
                    
                  </div>
                </div>
                <div class="table-responsive">
                  <table class="table" id="tabla_pedido">
                    <thead class=" text-primary">
                      <th>
                          {{ __('Producto') }}
                      </th>
                      <th>
                          {{ __('Nombre del cliente') }}
                      </th>
                      <th>
                          {{ __('Método de pago') }}
                      </th>
                      <th>
                          {{ __('Estado') }}
                      </th>
                      <th>
                          {{ __('Valor') }}
                      </th>
                      <th>
                        {{ __('Fecha de creación') }}
                      </th>
                      <th class="text-right">
                        {{ __('Acciones') }}
                      </th>
                    </thead>
                    <tbody>
                      @foreach($pedidos as $pedido)
                        <tr>
                          <td>
                            {{ $pedido->name }}
                          </td>
                          <td>
                            {{ $pedido->customer_name }}
                          </td>
                          <td>
                            {{ $pedido->payment_method }}
                          </td>
                          <td>
                            @if($pedido->status == 'APPROVED')
                            <div class="status green">
                            Aprobada
                            </div>
                            @endif
                            @if($pedido->status == 'PENDING')
                            <div class="status orange">
                            Pendiente
                            </div>
                            @endif
                            @if($pedido->status == 'REJECTED')
                            <div class="status red">
                            Rechazada
                            </div>
                            @endif
                          </td>
                          <td>
                            {{ $pedido->total }}
                          </td>
                          <td>
                            {{ $pedido->created_at }}
                          </td>
                          <td class="td-actions text-right">
                              <form action="{{ route('pedidos.destroy', $pedido->id)}}" style="display: inline-block;" method="post">
                              @csrf
                              @method('delete')
                          
                              <!-- <a rel="tooltip" class="btn btn-success btn-link" href="{{ route('pedidos.edit', $pedido) }}" data-original-title="" title="">
                                <i class="material-icons">edit</i>
                                <div class="ripple-container"></div>
                              </a> -->
                              <button type="button" class="btn btn-danger btn-link" data-original-title="" title="" onclick="confirm('{{ __("Estas seguro que deseas eliminar el pedido?") }}') ? this.parentElement.submit() : ''">
                                  <i class="material-icons">close</i>
                                  <div class="ripple-container"></div>
                              </button>
                             </form>
                          </td>
                        </tr>
                      @endforeach
                    </tbody>
                  </table>
                </div>
              




            </div>
        </div>
      </div>
    </div>
  </div>
@endsection