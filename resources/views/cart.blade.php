@extends('layout')
 
@section('title', 'Cart')
 
@section('content')
 
    <table id="cart" class="table table-hover table-condensed">
        <thead>
        <tr>
            <th style="width:50%">Producto</th>
            <th style="width:10%">Precio</th>
            <th style="width:8%">Cantidad</th>
            <th style="width:22%" class="text-center">Subtotal</th>
            <th style="width:10%"></th>
        </tr>
        </thead>
        <tbody>
 
        <?php $total = 0 ?>
 
        @if(session('cart'))
            @foreach(session('cart') as $id => $details)
 
                <?php $total += $details['price'] * $details['quantity'] ?>
 
                <tr>
                    <td data-th="Product">
                        <div class="row">
                            <div class="col-sm-3 hidden-xs"><img src="{{ asset('material') }}/img/products/{{ $details['photo'] }}" width="100" height="100" class="img-responsive"/></div>
                            <div class="col-sm-9">
                                <h4 class="nomargin">{{ $details['name'] }}</h4>
                            </div>
                        </div>
                    </td>
                    <td data-th="Precio">${{ $details['price'] }}</td>
                    <td data-th="Cantidad">
                        <input type="number" value="{{ $details['quantity'] }}" class="form-control quantity" />
                    </td>
                    <td data-th="Subtotal" class="text-center">${{ $details['price'] * $details['quantity'] }}</td>
                    <td class="actions" data-th="">
                        <button class="btn btn-info btn-sm update-cart" data-id="{{ $id }}"><i class="fa fa-refresh"></i></button>
                        <button class="btn btn-danger btn-sm remove-from-cart" data-id="{{ $id }}"><i class="fa fa-trash-o"></i></button>
                    </td>
                </tr>
            @endforeach
        @endif
 
        </tbody>
        <tfoot>
        <tr class="visible-xs">
            <td class="text-center"><strong>Total {{ $total }}</strong></td>
        </tr>
        <tr>
            <td><a href="{{ url('/') }}" class="btn btn-warning">Continuar comprando</a></td>
            <td colspan="2" class="hidden-xs"></td>
            <td class="hidden-xs text-center"><strong>Total ${{ $total }}</strong></td>
        </tr>
        </tfoot>
    </table>

    <form id="checkout" method="get" action="/cart/redirection" autocomplete="off" class="form-horizontal checkout">
        <div class="row">
            <div class="col-sm-12">
                <div class="row">
                    <div class="col-sm-3">
                        <div class="form-group">
                          <input type="hidden" name="sku" value="1111111">
                          <input type="hidden" name="nombre_producto" value="{{ $details['name'] }}">
                          <input type="hidden" name="cantidad" value="{{ $details['quantity'] }}">
                          <input type="hidden" name="precio" value="{{ $details['price'] }}">
                          <input type="hidden" name="total" value="{{ $total }}">
                          <input class="form-control" name="nombre" id="nombre" type="text" placeholder="Ingrese su nombre" value="" required="true" aria-required="true"/>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="form-group">
                          <input class="form-control" name="email" id="email" type="email" placeholder="Ingrese su correo electrónico" value="" required="true" aria-required="true"/>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="form-group">
                          <input class="form-control" name="celular" id="celular" type="number" placeholder="Ingrese su número de celular" value="" required="true" aria-required="true"/>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="form-group">
                          <button type="submit" name="checkout" class="btn btn-success">Proceder al pago</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
 
@endsection

@section('scripts')
 
 
    <script type="text/javascript">
 
        $(".update-cart").click(function (e) {
           e.preventDefault();
 
           var ele = $(this);
 
            $.ajax({
               url: '{{ url('update-cart') }}',
               method: "patch",
               data: {_token: '{{ csrf_token() }}', id: ele.attr("data-id"), quantity: ele.parents("tr").find(".quantity").val()},
               success: function (response) {
                   window.location.reload();
               }
            });
        });
 
        $(".remove-from-cart").click(function (e) {
            e.preventDefault();
 
            var ele = $(this);
 
            if(confirm("Are you sure")) {
                $.ajax({
                    url: '{{ url('remove-from-cart') }}',
                    method: "DELETE",
                    data: {_token: '{{ csrf_token() }}', id: ele.attr("data-id")},
                    success: function (response) {
                        window.location.reload();
                    }
                });
            }
        });


        $("#checkout").submit(function(event){
            event.preventDefault(); //prevent default action 
            var post_url = $(this).attr("action"); //get form action url
            var request_method = $(this).attr("method"); //get form GET/POST method
            var form_data = $(this).serialize(); //Encode form elements for submission
            
            $.ajax({
                url : post_url,
                type: request_method,
                data : form_data,
                success: function (response) {
                      location.href = response.proccess_url;
                }
            });
        });
 
    </script>
 
@endsection