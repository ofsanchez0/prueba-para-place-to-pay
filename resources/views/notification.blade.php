@extends('layout')
 
@section('title', 'Cart')
 
@section('content')
    
    <div class="notification">
        <div class="col-lg-12 col-sm-12 col-12">
            <div class="row">
                <div class="col-lg-2 col-sm-2 col-12">
                </div>
                <div class="col-lg-8 col-sm-8 col-12">
                    @if($transaction->status == 'APPROVED')
                        <h2>Su transacción ha sido aprobada</h2>
                    @endif
                    @if($transaction->status == 'PENDING')
                        <h2>Su transacción se encuentra pendiente de pago</h2>
                    @endif
                    @if($transaction->status == 'REJECTED')
                        <h2>Su transacción ha sido rechazada</h2>
                    @endif
                    <table id="cart" class="table table-hover table-condensed">
                        <thead>
                        <tr>
                            <th style="width:70%">PRODUCTO</th>
                            <th style="width:30%">TOTAL</th>
                        </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td><b>{{$transaction->name}}</b></td>
                                <td>$ {{$transaction->price}}</td>
                            </tr>
                            <tr>
                                <td><b>Subtotal:</b></td>
                                <td><span class="verde">$ {{$transaction->total}}</span></td>
                            </tr>
                            <tr>
                                <td><b>Envío:</b></td>
                                <td>Libre</td>
                            </tr>
                            <tr>
                                <td><b>Método de pago:</b></td>
                                <td>{{$transaction->payment_method}}</td>
                            </tr>
                            <tr>
                                <td><b><span class="big">Total:</span></b></td>
                                <td><b><span class="big verde">$ {{$transaction->total}}</span></b></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div class="col-lg-2 col-sm-2 col-12">
                </div>
            </div>
        </div>
        <div class="col-lg-12 col-sm-12 col-12">
            <div class="row">
                <div class="col-lg-12 col-sm-12 col-12">
                    @if($transaction->status == 'APPROVED')
                    <a style="text-align: center; display: table; margin: auto;" class="btn btn-info cart-button" href="/">REGRESAR A LA TIENDA</a>
                    @endif
                    @if($transaction->status == 'REJECTED')
                    <a style="text-align: center; display: table; margin: auto;" class="btn btn-info cart-button" href="/">REGRESAR A LA TIENDA</a>
                    @endif
                    @if($transaction->status == 'PENDING')
                    <a style="text-align: center; display: table; margin: auto;" class="btn btn-info cart-button" href="{{$transaction->process_url}}">CONTINUAR CON EL PAGO</a>
                    @endif
                </div>
            </div>
        </div>
    </div>

 
@endsection